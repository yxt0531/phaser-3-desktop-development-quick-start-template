#!/bin/bash
rm -R ./locales/
rm ./credits.html
rm ./icudtl.dat
rm ./nw_100_percent.pak
rm ./nw_200_percent.pak
rm ./resources.pak
rm ./v8_context_snapshot.bin
cp -R ./linux-x64/* ./
