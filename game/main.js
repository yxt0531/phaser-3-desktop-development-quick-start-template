var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: "arcade",
        arcade: {
            gravity: { y: 200 },
        },
    },
    scene: [Sample],
};

// Get the current window
var win = nw.Window.get();
win.resizeTo(config.width, config.height);
win.setPosition("center");

var game = new Phaser.Game(config);
