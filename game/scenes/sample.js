class Sample extends Phaser.Scene {
    constructor() {
        super({key: "Sample"});
        var fps;
        var dt;
    }

    preload() {
        // this.load.setBaseURL("http://labs.phaser.io");
        this.load.image("sky", "/game/space3.png");
        this.load.image("logo", "/game/phaser3-logo.png");
        this.load.image("red", "/game/red.png");
    }
    
    create() {
        
        this.add.image(400, 300, "sky");
    
        var particles = this.add.particles("red");
    
        var emitter = particles.createEmitter({
            speed: 100,
            scale: { start: 1, end: 0 },
            blendMode: "ADD",
        });
    
        var logo = this.physics.add.image(400, 100, "logo");
    
        logo.setVelocity(100, 200);
        logo.setBounce(1, 1);
        logo.setCollideWorldBounds(true);
    
        emitter.startFollow(logo);

        this.fps = this.add.text(0, 0, "FPS: 0", { fontFamily: 'Georgia, "Goudy Bookletter 1911", Times, serif' });
        this.dt = this.add.text(0, 18, "Delta Time: 0", { fontFamily: 'Georgia, "Goudy Bookletter 1911", Times, serif' });
    }

    update(time, delta) {
        this.fps.setText("FPS: " + this.game.loop.actualFps);
        this.dt.setText("Delta Time: " + delta);
    }
}
