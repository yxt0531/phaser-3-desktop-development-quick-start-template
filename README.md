# Phaser 3 Desktop Development Quick Start Template

This is a quick start template for building desktop game using Phaser 3.23.0 and NW.js v0.46.0.

Use *init-win-x64.bat* for initializing development on Windows, or *init-linux-x64.sh* on Linux.
